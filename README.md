# Updates #
##
[merging ipython (jupyter) notebooks video](https://www.youtube.com/watch?v=tKAmwC8ay8E&index=18&list=PLYx7XA2nY5Gf37zYZMw6OqGFRPjB1jCy6)
## Using Git to keep folder up-to-date
This is a quick and dirty way to use Git to keep things synched with the repo. I'll dedicate a lecture on how to use Git the proper way to keep things synched and even submit your own changes

- Install Git for your OS
- There are several GUI utilities that help you maintain your git repos visually: [Github Desktop](https://help.github.com/desktop/guides/getting-started/installing-github-desktop/) and [Source Tree](https://www.sourcetreeapp.com/)
- Once you have Git installed, clone my repo
    - Open a terminal and navigate to where you want the repo installed
    - type: git clone git@bitbucket.org:jtouma/python-workshop.git

This will create a folder on your computer named python-workshop. Now you can launch Jupyter Notebook and work with the notebooks. Before you start playing with the code, create your own branch (in this example I call the branch "branch1":

- from the terminal window, navigate to the directory where the repo is setup
- type: git checkout -b branch1
- now launch Jupyter notebook and play with the code in the notebooks

When you're ready to get the updates from the repo

- navigate to the local copy of the repo (using the command line)
- type: git commit -am "wip"
- type: git checkout master
- type: git pull
- type: git checkout -b branch2

Now branch2 contains all the new changes from the repo. I'm keeping it simple for the time being. During the git tutorial session I'll cover the proper way to work with Git repos. 


## Jun 28, 2016
- started talking about git

## Jun 22, 2016
- published HW01