{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Read and Write HDF5 from Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Contents\n",
    "1. Storing Data in HDF5 Files\n",
    "2. Reading Data from HDF5 Files\n",
    "3. Groups\n",
    "4. Compressed Data\n",
    "5. Reference Guide\n",
    "6. Further Reading"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Storing Data in HDF5 Files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The example below shows how to save two numpy arrays to a hdf5 file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from __future__ import print_function\n",
    "import numpy as np\n",
    "import h5py\n",
    " \n",
    "m1 = np.random.random(size = (1000,20))\n",
    "m2 = np.random.random(size = (1000,200))\n",
    " \n",
    "with h5py.File('data.h5', 'w') as hf:\n",
    "    hf.create_dataset('dataset_1', data=m1)\n",
    "    hf.create_dataset('dataset_2', data=m2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The line import h5py imports the corresponding module to handle hdf5 files. Afterwards a Numpy array called m1, with 1000 rows and 20 columns of random numbers, is created. The second array m2 has 1000 rows and 200 columns.\n",
    "\n",
    "The statement\n",
    "\n",
    "with h5py.File('data.h5', 'w') as hf:\n",
    "opens the file 'data.h5' in writing mode 'w' and stores it in the variable hf. This command makes sure that once you are done with the file it is closed properly, preventing errors.\n",
    "\n",
    "See the reference guide for a complete list of file modes.\n",
    "\n",
    "The line\n",
    "\n",
    "hf.create_dataset('dataset_1', data=m1)\n",
    "adds a dataset (you can think of it as a numpy array) to the file, this dataset is called 'dataset_1' and is generated out of the array m1. The second statement is analogous and creates the dataset 'dataset_2'.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading Data from HDF5 Files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To retrieve data out of a HDF5 file and manipulate it with the numpy methods you have to convert the HDF5 dataset to a numpy array, as shown in the example below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from __future__ import print_function\n",
    "import numpy as np\n",
    "import h5py\n",
    " \n",
    "with h5py.File('data.h5','r') as hf:\n",
    "    print('List of arrays in this file: \\n', hf.keys())\n",
    "    data = hf.get('dataset_1')\n",
    "    np_data = np.array(data)\n",
    "    print('Shape of the array dataset_1: \\n', np_data.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The file 'data.h5' was opened as read-only and stored in the variable hf by\n",
    "\n",
    "with h5py.File('data.h5','r') as hf:\n",
    "which also will properly close it once the indented code is executed. See the reference guide to learn about the file modes available.\n",
    "\n",
    "The rest of the commands are:\n",
    "\n",
    "print('List of arrays in this file: \\n', hf.keys()). Print a list of arrays in this file.\n",
    "data = hf.get('dataset_1'). Gets the data out of 'dataset_1'.\n",
    "np_data = np.array(data). Converts the data to a numpy array.\n",
    "print('Shape of the array dataset_1: \\n', np_data.shape). Prints the shape of the array."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Groups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data saved into HDF5 files can be organized in groups. Groups are like subfolders, they are the basic container mechanism."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from __future__ import print_function\n",
    "import numpy as np\n",
    "import h5py\n",
    " \n",
    "m1 = np.random.random(size = (1000,20))\n",
    "m2 = np.random.random(size = (1000,200))\n",
    "m3 = np.random.random(size = (1000,1000))\n",
    " \n",
    "with h5py.File('data.h5', 'w') as hf:\n",
    "    g1 = hf.create_group('group1')\n",
    "    g1.create_dataset('dataset1', data = m1)\n",
    "    g1.create_dataset('dataset3', data = m3)\n",
    " \n",
    "    g2 = hf.create_group('group2/subgroup')\n",
    "    g2.create_dataset('dataset2', data = m2)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example the file \"data.h5\" is created, inside the base directory of that file we create a group called 'group1' and save it to the g1 variable. Inside that group we put two datasets, 'dataset1' and 'dataset2' which contain the data from the random matrices m1 and m3, respectively.\n",
    "\n",
    "After that we create a nested group 'group2/subgroup', if we want we can access 'group2' and store data there or, as in the example, store the data in the 'subgroup'. We save the m2 matrix into a dataset called 'dataset2'.\n",
    "\n",
    "Just as in the previous section, we can use get() to read the data from directories and subdirectories:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "with h5py.File('data.h5','r') as hf:\n",
    "    print('List of items in the base directory:', hf.items())\n",
    "    gp1 = hf.get('group1')\n",
    "    gp2 = hf.get('group2/subgroup')\n",
    "    print(u'\\nlist of items in the group 1:', gp1.items())\n",
    "    print(u'\\nlist of items in the subgroup:', gp2.items())\n",
    " \n",
    "    arr1 = np.array(gp1.get('dataset3'))\n",
    "    print(arr1.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The relevant commands are:\n",
    "\n",
    "hf.items(). Shows a list of the elements in the base directory and their corresponding type.\n",
    "gp1 = hf.get('group1'). Reads the information about 'group1' and stores it in gp1\n",
    "gp1.items(). Shows a list of the elements in gp1 and their corresponding types (and shapes).\n",
    "arr1 = np.array(gp1.get('dataset3')). Reads the information from 'dataset3' and converts it into a numpy array, which is assigned to arr1.\n",
    "The get() routine returns a h5py dataset, which supports slicing, fancy indexing and some other operations; but in most cases is easier to convert it to a numpy array since those are widely used in many python modules."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compressed Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are two additional parameters that can be used to compress the data in the way to the hard drive. The compression is transparent to the user and won't affect the reading commands, although reading zipped files might be slightly slower."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from __future__ import print_function\n",
    "import numpy as np\n",
    "import h5py\n",
    " \n",
    "m1 = np.random.random(size = (1000,20))\n",
    "m2 = np.random.random(size = (1000,200))\n",
    "m3 = np.random.random(size = (1000,1000))\n",
    " \n",
    "with h5py.File('data.h5', 'w') as hf:\n",
    "    g1 = hf.create_group('group1')\n",
    "    g1.create_dataset('dataset1', data = m1, compression=\"gzip\", compression_opts=9)\n",
    "    g1.create_dataset('dataset3', data = m3, compression=\"gzip\", compression_opts=9)\n",
    " \n",
    "    g2 = hf.create_group('group2/subgroup')\n",
    "    g2.create_dataset('dataset2', data = m2, compression=\"gzip\", compression_opts=9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The arguments compression=\"gzip\", compression_opts=9 set the compression filter to \"gzip\" and the compression level to 9. The level can be an integer from 0 to 9, the compression filters available are: \"gzip\" and \"lzf\".\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Refereces\n",
    "File modes and description\n",
    "\n",
    "r. Read only, the file must exist.\n",
    "r+. Read and write, the file must exist.\n",
    "w. Create file, overwrite if it exists.\n",
    "x. Create file, fail if it exists.\n",
    "a. Read and write, create otherwise.\n"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [Root]",
   "language": "python",
   "name": "Python [Root]"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
