#!/usr/bin/env python



# run as python srp.py folder_name output_csv_file_name


import sys
import glob
import os
import os.path
import sys
import csv



def get_dash_idx(lines):
    dash_idx = list()
    for idx, line in enumerate(data):
        _line = line.strip()
        if '-----' in _line:
            dash_idx.append(idx)

    return dash_idx



# master list
master_list = list()

# header list
header = list()

# get a listing of the directory
files = sorted(glob.glob(os.path.join(sys.argv[1], '*.srp')))

# assuming all the files have the same structure
# open a file and get the keys the intitialize the keys to empty lists
with open(files[0], 'r') as fin:
    data = fin.readlines()

dash_idx = get_dash_idx(data)

header.append('time')
for idx in range(dash_idx[0]+1, dash_idx[1]):
    line = data[idx].strip().split()
    header.append(line[0])

line = data[dash_idx[1]+1].strip().split()
header.append(line[0])



# loop through the files and populate the dict
for _file in files:
    # dictionary per file
    vals = dict()
    
    with open(_file, 'r') as fin:
        data = fin.readlines()

    dash_idx = get_dash_idx(data)
    
    _time = float(os.path.splitext(os.path.basename(_file))[0])
    vals['time'] = _time

    for idx in range(dash_idx[0]+1, dash_idx[1]):
        line = data[idx].strip().split()
        vals[line[0]] = float(line[1])

    line = data[dash_idx[1]+1].strip().split()
    vals[line[0]] = float(line[1])

    master_list.append(vals)




with open(sys.argv[2], 'wb') as fout:
    dict_write = csv.DictWriter(fout, header)
    dict_write.writeheader()
    dict_write.writerows(master_list)
